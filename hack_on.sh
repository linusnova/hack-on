#!/usr/share/hack_on

#Colours
green="\e[0;32m\033[1m"
end="\033[0m\e[0m"
red="\e[0;31m\033[1m"
blue="\e[0;34m\033[1m"
yellow="\e[0;33m\033[1m"
purple="\e[0;35m\033[1m"
turquoise="\e[0;36m\033[1m"
gray="\e[0;37m\033[1m"


trap ctrl_c INT

function ctrl_c(){
	clear; echo -e "${yellow}[!]${end} Estás seguro de salir del script?${end}"
	read -p "si = s / no = n >>" check

	if [ $check = s ]; then
		clear; exit 0
	elif [ $check = n ]; then
		startPanel
	else
		startPanel
	fi
}

function dependencies(){
	clear; dependencies=(git toilet python2 php openssh httrack)

	echo -e "${yellow}[!]${end} checando dependencies de los programas necesarios"
	sleep 2

	for program in ${dependencies[@]}; do
		echo -ne "\t${blue}$program...${end}\t"
		test -f /data/data/com.termux/files/usr/bin/$program

		if [ "$(echo $?)" == "0" ]; then
			echo -e "${blue}(${end}${green}ok${end}${blue})${end}"
		else
			echo -e "${blue}(${end}${red}wrong${end}${blue})${end}"
			pkg install $program -y;
		fi; sleep 1
	done
}

function helpPanel(){
	echo -e "\n${yellowColour}[*]${endColour}${grayColour} Uso: ./hack_on.sh${endColour}"
	echo -e "\n\t${purpleColour}a)${endColour}${yellowColour} Modo de ataque${endColour}"
	echo -e "\t\t${redColour}kenndra${endColour}"
	echo -e "\t\t${redColour}keyla${endColour}"
	exit 0
}

function starter(){
	dependencies=(STARTER)
	
	echo -e "${red}(${end}${blue}!${end}${red})${end}${yellow} Instalando parche de seguridad de \033[5m Hack_on \033[5m${end}"
	sleep 2
	for program in ${dependencies}; do
		echo -e "$program"
		test -f $program
		if [ "$(echo $?)" == 0 ]; then
			rm /data/data/com.termux/files/usr/etc/motd && touch /data/data/com.termux/files/share/etc/motd;
			echo 'toilet -f pagga hack on --filter border' >> /data/data/com.termux/files/etc/bash.bashrc;
			echo -e " sin instalar"
			rm STARTER
			./termux-desktop/setup.sh --install
		else
			echo -e " ya instalado"
			startPanel
		fi; sleep 1
	done
}

function startPanel(){
	clear; echo -e "${red}bienvenido ${end}$name_data"
	toilet -f pagga hack on --filter border;
	echo ""
	echo -e "\t${red}[1]${end} dependencias${end}\t"
	echo -e "\t${red}[2]${end} phishing${end}\t"
	echo -e "\t${red}[3]${end} fuerza bruta${end}\t"
	echo -e "\t${red}[4]${end} doxeo${end}\t"
	echo -e "\t${red}[5]${end} hackear cámara${end}\t"
	echo -e "\t${red}[6]${end} instalar ngrok${end}\t"
	echo ""
	setterm --foreground green
	read -p "(hack_on@konsole)>> " ch
	setterm --foreground white

	if [ $ch = 1 ]; then
		dependencies
	elif [ $ch = 2 ]; then
		cd weeman; python2 weeman.py
	elif [ $ch = 3 ]; then
		bash SocialBox/SocialBox.sh
	elif [ $ch = 4 ]; then
		bash doxeo/gvngs.sh
	elif [ $ch = 5 ]; then
		bash saycheese/saycheese.sh
	elif [ $ch = 6 ]; then
		bash ngrok/termux-ngrok.sh
	elif [ $ch = 7 ]; then
		bininstall
	else
		echo -e "${red}opción no reconocida${end}"
	fi
}

function bininstall(){
	dependencies=(hack_on)

	echo -e "checando la existencia del archivo bin"
	sleep 2
	for program in "${dependencies[@]}"; do
			echo -ne "$program"
			test -f /data/data/com.termux/files/usr/share/bin/$program

			if [ "$(echo $?)" == "0" ]; then
				echo -e "ok"
			else
				echo -e "false"
				cp -r hack_on.sh /data/data/com.termux/files/usr/share/ ;
				touch /data/data/com.termux/files/usr/bin/hack_on ;
				echo 'bash /data/data/com.termux/files/usr/share/hack_on.sh' >> /data/data/com.termux/files/usr/bin/hack_on;
				bininstall
			fi; sleep 1
	done
}

declare -i parameter_counter=0; while getopts ":n:m:h:" arg; do
	case $arg in
		n) name_data=$OPTARG; let parameter_counter+=1 ;;
		m) mode_data=$OPTARG; let parameter_counter+=1 ;;
		h) helpPanel ;;
	esac
done

if [ $parameter_counter -ne 2 ]; then
	helpPanel
else
	dependencies; starter
fi
